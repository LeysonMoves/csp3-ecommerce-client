import { Fragment, useEffect, useState } from 'react';
import ProductCardAdmin from '../components/ProductCardAdmin';
import { Container, Row } from 'react-bootstrap';


export default function Products(){
	

	// State that will be used to store the courses retrieved from the database
	const [products, setProducts] = useState([]);		

	useEffect(() => {
		fetch('https://glacial-cliffs-89609.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map((product,i) => {
					return (
						<ProductCardAdmin key={i} productProp={product} />
					)
				})
			);		
		})
	}, [])

	

	return (

		<Container>
		<Fragment> 
		<h1 className="mt-3 mb-4 title" >Products Database</h1>
		</Fragment>
		<Row>

		<Fragment> 
			{products}
		</Fragment>
		</Row>
		</Container>
	)
}
