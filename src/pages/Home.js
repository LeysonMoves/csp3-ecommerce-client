import { Fragment } from 'react';
import Banner from '../components/Banner';

import { Container } from 'react-bootstrap';



export default function Home(){
	const data={
		title: "WELCOME TO LM STREET LAB!",
		destination: "/products",
		label: "Shop now"

	}

	return(
			
		<Fragment>
			<Container className="home">
			<Banner data={data}/>
			</Container>
		
			
			
		</Fragment>

	)
}





// <Jumbotron fluid>
//   <Container>
//     <h1>Fluid jumbotron</h1>
//     <p>
//       This is a modified jumbotron that occupies the entire horizontal space of
//       its parent.
//     </p>
//   </Container>
// </Jumbotron>