import { useState, useEffect, useContext } from 'react';
import { Form, Button,Col, Row } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props) {
        // Allows us to consume the User context object and it's properties to use for user validation
        const {user, setUser} = useContext(UserContext);
        // State hooks to store the values of the input fields
        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        // State to determine whether submit button is enabled or not
        const [isActive, setIsActive] = useState(false);
        const[isAdmin, setIsAdmin]=useState(false);
        const history = useHistory();

        function authenticate(e) {
            e.preventDefault();

            // Fetch request to process the backend API
          
            fetch('https://glacial-cliffs-89609.herokuapp.com/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // If no user information is found, the "access" property will not be available and will return undefined
                if(typeof data.access !== "undefined"){
                    // The token will be used to retrieve user information across the whole frontend application and storing it in the localStorage to allow ease of access to the user's information
                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);

                                   }

                    else{
                    Swal.fire({
                        title: "Incorrect email or password",
                        icon: "error",
                        text: "Please try again.",
                        confirmButtonColor: 'black'
                    })
                }
            })
            
            setEmail('');
            setPassword('');

        }

        const retrieveUserDetails = (token) => {
            fetch('https://glacial-cliffs-89609.herokuapp.com/users/details', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }

        useEffect(() => {

            // Validation to enable submit button when all fields are populated and both passwords match
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);



    return (
            (user.id !== null) ?
            (user.isAdmin ===true)?

                <Redirect to="/admin" />
                :
                <Redirect to="/" />

            :
            
            <Form onSubmit={(e) => authenticate(e)}>
            <h1 className = "title">Login</h1>
           <Form.Group >

                <Row>
                <Col xs={12} md={6}  className ="login">
                <Form.Group controlId="userEmail">
                    <Form.Label className="label">Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                       
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required />
                </Form.Group>
                 </Col>

                <Col xs={12} md={6}  className ="login">
                <Form.Group controlId="password">
                   <Form.Label className="label">Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required />
                </Form.Group>
                </Col>
                </Row>

                <Col className=" loginButton d-flex justify-content-center">
                { isActive ? 
                    <Button variant="dark" type="submit" id="submitBtn">
                        Sign In
                    </Button>
                    : 
                    <Button   variant="secondary" type="submit" id="submitBtn" disabled>
                        Sign In
                    </Button>
                }
              </Col> 
              </Form.Group>

            </Form>
            

        )  
}
