import { Row, Col, Card, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
export default function Admin(){
		
	return (

		<Row >
		<Col xs={12} md={12} className="mt-1 mb-3">
		<h1 className="title">Admin Dashboard</h1>
		</Col>
			
			<Col xs={12} md={4}>
				<Card className=" border-0">
				  
				  <Card.Body className="adminCard mt-3">
				    <Card.Title>
				    <h3 className="admintitle">Show All Products</h3>	
				    </Card.Title>
				    
				    <div className="d-flex justify-content-center">
				   	<Button className="adminButton"variant="dark" type="submit" as={NavLink}  to="/productsDatabase">View All Products</Button> 	
				   	</div>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="  border-0">
				  
				  <Card.Body className="adminCard mt-3">
				    <Card.Title>
				    <h3 className="admintitle">Add New Product</h3>
				    </Card.Title>
				 
				    <div className="d-flex justify-content-center">
				    <Button className="adminButton"variant="dark" type="submit" as={NavLink}  to="/create">Add Product</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="  border-0">
				  
				  <Card.Body className="adminCard mt-3">
				    <Card.Title>
				    <h3 className="admintitle">Show All Users</h3>
				    </Card.Title>
				    
				    <div className="d-flex justify-content-center">
				    <Button className="adminButton"variant="dark"  type="submit" as={NavLink}  to="/users">User Database</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="  border-0" >
				  
				  <Card.Body className="adminCard mt-3">
				    <Card.Title>
				    <h3 className="admintitle">Show All Orders</h3>
				    </Card.Title>
				    
				    <div className="d-flex justify-content-center">
				    <Button className="adminButton"variant="dark"  type="submit" as={NavLink}  to="/orders">Order Database</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className=" border-0">
				  
				  <Card.Body className="adminCard mt-3">
				    <Card.Title>
				    <h3 className="admintitle">Show All Carts</h3>
				    </Card.Title>
				    
				    <div className="d-flex justify-content-center">
				    <Button className="adminButton"variant="dark"  type="submit" as={NavLink}  to="/cartDatabase">Go to Active Carts</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className=" border-0">
				  
				  <Card.Body className="adminCard mt-3">
				    <Card.Title>
				    <h3 className="admintitle">Show Analytics</h3>
				    </Card.Title>
				    
				    <div className="d-flex justify-content-center">
				    <Button className="adminButton"variant="dark"  type="submit" as={NavLink}  to="#">Check Reports</Button>
				    </div>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}


