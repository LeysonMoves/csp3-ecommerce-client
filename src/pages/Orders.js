import { Fragment, useEffect, useState } from 'react';
import OrderCard from '../components/OrderCard';
import { Container, Row,Button,Col,Table } from 'react-bootstrap';


export default function Orders(){
		const [orders, setOrders] = useState([]);
			

		useEffect(() => {
		fetch('https://glacial-cliffs-89609.herokuapp.com/orders/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setOrders(data.map(order => {
				return (
				<OrderCard key ={order.id} orderProp ={order} />
			)
				})
			);		
		})
	}, [])

	

	
	return (
		<div>
			
		<Fragment> 
		<h1 className="mt-3 mb-4 title" >All Orders</h1>
		</Fragment>
		<Fragment>
		   <Table className="userTable" striped bordered hover variant="dark">
		   <thead >
	        <tr >
	          <th  >Order ID</th>
	          <th >Product Name</th>	
	          <th>Total Amount</th>
	          <th>Purchase Date</th>
	          <th>User ID</th>
	        </tr>
	      </thead>
	      <tbody>
	      </tbody>
	      </Table>
			{orders}
		</Fragment>
		
		
		</div>
	)
}
		
