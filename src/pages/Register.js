import { useState, useEffect, useContext } from 'react';
import { Form, Button,Row, Col} from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

    const {user} = useContext(UserContext);
    const history = useHistory();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);

    // Function to simulate user registration
    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('https://glacial-cliffs-89609.herokuapp.com/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch('https://glacial-cliffs-89609.herokuapp.com/users/register', {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to LM STREET LAB!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        history.push("/login");

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);

    return (
        (user.id !== null) ?
            <Redirect to="/products" />
        :
        
            <Form  onSubmit={(e) => registerUser(e)}>
                
                <h1 className="title">Register</h1>
                
                <Row>
                 <Col xs={12} md={6}  className ="login">
                <Form.Group controlId="firstName">
                    <Form.Label className="label">First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                       
                        value={firstName} 
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>
                </Col>
                 <Col xs={12} md={6}  className ="login">
                <Form.Group controlId="lastName">
                    <Form.Label className="label">Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={lastName} 
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                    
                </Form.Group>
                </Col>
                

                <Col xs={12} md={6}  className ="login">
                <Form.Group controlId="userEmail">
                    <Form.Label className="label">Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        value={email} 
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                    
                </Form.Group>
                </Col>

                <Col xs={12} md={6}  className ="login">    
                <Form.Group controlId="mobileNo">
                    <Form.Label className="label">Mobile Number</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={mobileNo} 
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>
                </Col>

                <Col xs={12} md={6}  className ="login">
                <Form.Group controlId="password1">
                    <Form.Label className="label">Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        value={password1} 
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>
                </Col>

                <Col xs={12} md={6}  className ="login">
                <Form.Group controlId="password2">
                    <Form.Label className="label">Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        value={password2} 
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>
                </Col>

                </Row>

                <Col className="d-flex justify-content-center">
                { isActive ? 
                    <Button className="registerButton" variant="dark " type="submit" id="submitBtn">
                        Create Account
                    </Button>
                    : 
                    <Button className="registerButton" variant="secondary" type="submit" id="submitBtn" disabled>
                        Create Account
                    </Button>
                }
                </Col>
            </Form>
    )
    
}
