import { useState, useEffect, useContext } from 'react';
import React from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

export default function AddProduct(){

    const {user, setUser} = useContext(UserContext);
    // State hooks to store the values of the input fields
    const [image, setImage] = useState('');
    const [name, setName] = useState('');
 
    const [price, setPrice] = useState('');
 
    let history = useHistory();

 

    // Function to simulate adding product
    async function AddProduct(e){
          e.preventDefault();
          fetch('https://glacial-cliffs-89609.herokuapp.com/products/create',{
            method: 'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                name:name,
                image:image,
                
                price:price
            })
          })
          .then(res => res.json())
          .then(data => {
                      
                Swal.fire({
                    title:"Product added",
                    icon:"success",             
                    confirmButtonColor: 'black',

                });
                history.push('/create')
              })
}

return (    

        <Row >

        <Col xs={0} md={4} > </Col>
       
        <Col xs={12} md={4}> 
         <h2 className="title">Add A Product</h2>
        <Form classname="form"onSubmit={(e) => AddProduct(e)}>
                <Form.Group controlId="productName">
                <Form.Label className="label">Product Name</Form.Label>
                <Row className="field">
                <Col   >
                <Form.Control size="sm"
                    type="productname" 
                    placeholder="" 
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required />
                </Col>
                </Row>
                </Form.Group>


                <Form.Group controlId="productName">
                <Form.Label className="label">Product Url</Form.Label>
                <Row>
                <Col   >
                <Form.Control size="sm"
                    type="productimage" 
                    placeholder="" 
                    value={image}
                    onChange={e => setImage(e.target.value)}
                    required />
                </Col>
                </Row>
                <Button className="uploadButton" variant="success" >Upload A File</Button>
                </Form.Group>


                <Form.Group controlId="productPrice">
                <Form.Label className="label">Price</Form.Label>
                <Row>
                <Col   >
                <Form.Control size="sm" 
                    type="productprice" 
                    placeholder="" 
                    value = {price}
                    onChange = { e => setPrice(e.target.value)}
                    required />
                </Col>

                </Row>

                </Form.Group>
                <Col className="mt-3 d-flex justify-content-center">
                    <Button className="adminButton" variant="dark btn-block" type="submit" id="submitBtn">
                       Post Your Product
                    </Button>
                    </Col>
                
                  
        </Form>
        </Col>
        
          <Col xs={0} md={4} > </Col>
        </Row>
    )
}



