import { Fragment, useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import { Container, Row } from 'react-bootstrap';


export default function Products(){
	

	// State that will be used to store the courses retrieved from the database
	const [products, setProducts] = useState([]);		

	useEffect(() => {
		fetch('https://glacial-cliffs-89609.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map((product,i) => {
					return (
						<ProductCard key={i} productProp={product} />
					)
				})
			);		
		})
	}, [])

	

	return (

		<Container>
		<Fragment> 
		<h1 className="mt-3 mb-4 title" >All Products</h1>
		</Fragment>
		<Row>

		<Fragment> 
			{products}
		</Fragment>
		</Row>
		</Container>
	)
}
