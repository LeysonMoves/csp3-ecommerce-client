import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

export default function CourseView(props) {
	
	const productId = props.match.params.productId;
	const [name, setName] = useState([]);
	const [price, setPrice] = useState([]);
	const [image, setImage] =useState([]);
	
	const[cart,setCart] =useState([]);
	const[product,setProduct] =useState([]);

	const addToCart = (product) =>{
		setCart([...cart,product]);
	}


	useEffect(() => {
		fetch(`https://glacial-cliffs-89609.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setName(data.name);
				setImage(data.image);
				setPrice(data.price);
				}
			);
	}, [])
	

	return(
		<Container className = "mt-2">
		<Row>
		<header></header>
		<Col xs={0} md={3} > </Col>
			<Col xs={12} md={4}  className="ml-5">
				<Card style={{width: '25rem'} }>
					<Card.Body className = "text-center">
					<Card.Img  className="imageProduct" variant="top" src={image} />   
						<Card.Title className="label">{name}</Card.Title>

						<Card.Text>PHP{price}</Card.Text>
						
						<Button className="addToCartButton p-2" variant ="dark"   onClick={() => addToCart(product)}>Add to Cart</Button> 

					</Card.Body>
					
					<Button variant="warning mb-5">My Cart ({cart.length})</Button>
					
				</Card>
			</Col>
			<Col xs={0} md={2} > </Col>
			</Row>
		</Container>

	)
}