import { Fragment, useEffect, useState } from 'react';
import HistoryTable from '../components/HistoryTable';
import { Container, Row,Button,Col,Table } from 'react-bootstrap';


export default function Users(){
		const [users, setUsers] = useState([]);
	

		useEffect(() => {
		fetch('https://glacial-cliffs-89609.herokuapp.com/users/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setUsers(data.map(user => {
				return (
				<HistoryTable key ={user.id} userProp ={user} />
			)
				})
			);		
		})
	}, [])

	

	
	return (
		<div>
			
		<Fragment> 
		<h1 className="mt-3 mb-4 title" >Previous Orders</h1>
		</Fragment>
		<Fragment>
		   <Table className="userTable" striped bordered hover variant="dark">
		   <thead >
	        <tr >
	          <th>Product Name</th>
	          <th >Order ID</th>	
	          <th>Total Amount</th>
	          <th>Pruchase Date </th>
	         
	        </tr>
	      </thead>
	      <tbody>
	      </tbody>
	      </Table>
			{users}
		</Fragment>
		
		
		</div>
	)
}
		

