import { Fragment, useEffect, useState } from 'react';
import UserCard from '../components/UserCard';
import {  Table } from 'react-bootstrap';


export default function Users(){
		const [users, setUsers] = useState([]);
	

		useEffect(() => {
		fetch('https://glacial-cliffs-89609.herokuapp.com/users/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setUsers(data.map((user,i) => {
				return (
				<UserCard key ={i} userProp ={user} />
			)
				})
			);		
		})
	}, [])

	

	
	return (
		<div>
			
		<Fragment> 
		<h1 className="mt-3 mb-4 title" >All Users</h1>
		</Fragment>
		<Fragment>
		   <Table className="tablehead" striped bordered hover variant="dark">
		   <thead >
	        <tr >
	          <th className="tabeName">Full Name</th>
	          
	          <th>Email</th>
	          <th>User Id </th>
	          <th>Orders</th>
	        </tr>
	      </thead>
	      <tbody>
	      </tbody>
	      </Table>
			{users}
		</Fragment>
		
		
		</div>
	)
}
		

