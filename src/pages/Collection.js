import { Fragment } from 'react';
import Banner from '../components/Banner';

import { Container ,Carousel, Col, Row} from 'react-bootstrap';



export default function Collection(){


	return(
		<Container className="collectionBody">
		<h1 className="title pt-3">Collections</h1>
		<Row  >
		<Col xs={0} md={2} > </Col>
		<Col xs={12} md={4} className="mt-3">
		<Carousel className="collection" style={{width: '45rem'} }>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://scontent.fceb2-1.fna.fbcdn.net/v/t39.30808-6/259089159_10216544790731594_8040185441408730987_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=0debeb&_nc_eui2=AeFOwFR2DDMQ8Wf5vXNiTJdZD0gM0mlY9isPSAzSaVj2K5o6ADSOdNZNPpZoE070yfQ&_nc_ohc=VQgb-rcfUtQAX-QJBoX&_nc_ht=scontent.fceb2-1.fna&oh=8c4e4e84e1760cd97ce13ffc79d63d7d&oe=61A09C56"
		      alt="First slide"
		    />
		    
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://scontent.fceb2-1.fna.fbcdn.net/v/t39.30808-6/259473855_10216544790411586_5372396226767179791_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=0debeb&_nc_eui2=AeEsbCyMKDAPkrRX6xjPLt-FXva4RaoF2ete9rhFqgXZ6wRtqoKPdD7FV3TK9ICGSBs&_nc_ohc=Uwv1RzSD51wAX-KeZF2&_nc_ht=scontent.fceb2-1.fna&oh=2705ce6f8329b6e31dc4078bcfb61dff&oe=61A0DFAF"
		      alt="Second slide"
		    />

		    
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://scontent.fceb2-2.fna.fbcdn.net/v/t39.30808-6/259761612_10216544793371660_414289961787252253_n.jpg?_nc_cat=110&ccb=1-5&_nc_sid=0debeb&_nc_eui2=AeHdDXT8RusYzA3qi0cTGdbVJeqHX_8XIAkl6odf_xcgCY9ra7SHlT36TEI7SEIrFHo&_nc_ohc=0VHT3Cp0NioAX9ZwD-9&_nc_ht=scontent.fceb2-2.fna&oh=dd628edd5593c40b7a490e2fd1ccbc25&oe=619FD23A"
		      alt="Third slide"
		    />

		    
		  </Carousel.Item>
		</Carousel>
		</Col>
		<Col xs={0} md={4} > </Col>
		</Row>
		</Container>
	)
}



