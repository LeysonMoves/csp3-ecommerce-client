import { Container } from 'react-bootstrap';
import { useState, useEffect, Fragment} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Admin from'./pages/Admin';
import Logout from './pages/Logout';
import AddProduct from './pages/AddProduct';
import Users from './pages/Users';
import History from './pages/History';
import Collection from './pages/Collection';
import Error from './pages/Error';

import Orders from './pages/Orders';
import Cart from './pages/Cart';
import ProductView from './pages/ProductView';
import ProductDatabase from './pages/ProductDatabase';

import { UserProvider } from './UserContext';
import './App.css';




function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  },[user])
 

 
  return (
      <UserProvider value={{user, setUser, unsetUser}}>

      <Router>
        <AppNavbar/>
      
       
        <Container>
          <Switch>
            

           <Route exact path="/" component={Home} />
            <Route exact path="/products" component={Products} />      
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
             <Route exact path="/collection" component={Collection} />
             <Route exact path="/products/:productId" component={ProductView} /> 
             
              <Route exact path="/history" component={History} /> 
              <Route exact path="/cart" component={Cart} />
              <Route exact path="/logout" component={Logout} />

            {(user.isAdmin  === true)?
              <Fragment>
            <Route exact path="/create" component={AddProduct} />
             <Route exact path="/productsDatabase" component={ProductDatabase} />    
            <Route exact path="/admin" component={Admin} />
            <Route exact path="/orders" component={Orders} />
            <Route exact path="/users" component={Users} />
            <Route exact path="/cartDatabase" component={Cart} />
             
             
            

            </Fragment>

            :
            <Route component={Error} />
            }

          </Switch>
        </Container>
      </Router>
      </UserProvider>



  );
}

export default App;



