import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){
	console.log(data)
	const {title, content, destination, label} = data;
	
		return (
			
			<Row >
				<Col className="p-3">
					<h1 className="title">{title}</h1>
					<p>{content}</p>
					{/*<Link to={products}>{label}</Link>*/}
					<Col  className= "d-flex justify-content-center m-3">
					<Link to = {destination}>
					<Button className= "shopNowButton" >{label}</Button>
					</Link>
					</Col>
				</Col>
			</Row>
			
		)
}
