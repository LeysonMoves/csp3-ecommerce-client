import React from 'react';
import { useEffect, useState } from 'react';
import { Card,Button,Col, Row, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function OrderCard ({orderProp}){

    const{_id, name, totalAmount, purchasedOn,order} = orderProp;

    return ( 

	    <Table className="tableData" striped bordered hover variant="light" responsive="sm">
	   
	      <tbody >
	        <tr>         
	          <td className= "orderData">{_id}</td>
	          <td className= "orderData">{name}</td>
	          <td className= "orderData">Php{totalAmount}</td>
	          <td className= "orderData">{purchasedOn}</td>
	          <td className= "orderData">ORDERS</td>

	         
	        </tr>
	      
	      </tbody>
	    </Table>
	    
       
    )

}