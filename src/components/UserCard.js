
import React from 'react';
 
import { useEffect, useState } from 'react';
import { Card,Button,Col, Row, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function UserCard ({userProp}){

    const{firstName, lastName, email, orders, _id} = userProp;

    console.log(userProp);


    return ( 

	    <Table className="td" striped bordered hover variant="light" responsive="sm">
	   
	      <tbody >
	        <tr>         
	          <td className= "td">{firstName}  {lastName}</td>
	          <td className= "td">{email}</td>
	          <td className= "td">{_id}</td>
	          <td className= "td">[orders]</td>

	        </tr>
	      
	      </tbody>
	    </Table>
	    
       
    )

}

