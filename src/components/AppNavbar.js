import { Fragment, useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink,} from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar(){
	

	const { user } = useContext(UserContext);

	return(
		(user.isAdmin === true)?

		<Navbar className= "sticky-top" bg="dark" variant="dark" expand="lg">
		   <Navbar.Brand  as ={NavLink} to="/" exact><div className="brand" fw-bold >LM STREET LAB</div></Navbar.Brand>
		   <Navbar.Toggle aria-controls="basic-navbar-nav" />
		   <Navbar.Collapse id="basic-navbar-nav">
		    	<Nav className="navbarPage ml-auto">
		      	   <Nav.Link as={NavLink} to="/admin" exact>Admin</Nav.Link>
		     	   <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		    	</Nav>
		    </Navbar.Collapse>
		</Navbar>
	
		:

		<Navbar className= "sticky-top" bg="dark" variant="dark" expand="lg">
		   <Navbar.Brand  as ={NavLink} to="/" exact><div className="brand" fw-bold >LM STREET LAB</div></Navbar.Brand>
		   <Navbar.Toggle aria-controls="basic-navbar-nav" />
		   <Navbar.Collapse id="basic-navbar-nav">
		    	<Nav className="navbarPage ml-auto">
		      	   <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
		     	   <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
		     	   <Nav.Link as={NavLink} to="/collection" exact>Collections</Nav.Link>
		     	   {(user.id !== null) ? 

		     	   	<Fragment>
		     	   	 <Nav.Link as={NavLink} to="/cart" exact>My Cart</Nav.Link>
		     	   	 <Nav.Link as={NavLink} to="/history" exact>Order History</Nav.Link>
		     	   	 <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		     	   	 </Fragment>
		     	   	 :
		     	   	 <Fragment>
		     	   	 	<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		     	   	 	<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		     	   	 	
		     	   	 </Fragment>	
		     	   	}
		    	</Nav>
		    </Navbar.Collapse>
		</Navbar>

	)
}


