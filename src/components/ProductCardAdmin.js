
import React from 'react';
 
import { useEffect, useState } from 'react';
import { Card,Button,Col, Row } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

export default function ProductCard ({productProp}){
    const{_id,name,price, image} = productProp;

    return ( 
    
                <Row xs={12} md={3} lg={3} className=" allProduct d-flex justify-content-center m-3">
                <Col >
                <Card className="border-0" style={{width: '12rem'} }>
                <Card.Img  className="imageProduct" variant="top" src={image} />   
                    <Card.Body className=" allProduct" >     
                          
                       <Card.Title className="itemName">{name}</Card.Title>
                       <Card.Text className="itemPrice">PHP {price}</Card.Text>                                     
                       <button className="editProductButton p-2" type="submit">Edit Product</button>
                      
                     </Card.Body>     
                </Card> 
            </Col>
        </Row>
       
    )

}



